<?php get_header(); ?>
		<section class="container">
			<div class="row">
				
				<div class="content--body col-lg-12">					
					<?php the_post(); ?>
					<?php the_content(); ?>
				</div>
			</div>
		</section>
		
		<section class="container text_content">
			<div class="row">
				<div class="col-12">
					<div class="text_content--body">
						<?php if(get_field('text_content')){
							the_field('text_content');
						} ?>
					</div>
				</div>
			</div>
		</section>

<?php get_footer(); ?>