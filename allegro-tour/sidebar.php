<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
	<aside id="secondary" class="widget-area col-sm-12 col-lg-3" role="complementary">
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</aside><!-- #secondary -->
<?php else: ?>
	<aside id="secondary" class="widget-area col-sm-12 col-lg-3" role="complementary">
		<div id="left"> 		
			<div class="moduletable">
				<h3>Наш автопарк</h3>
				<div class="ajandar-block-wrapper" id="transport_type">
					<div id="type" style="background:url(/i/ajandar-block-img1.jpg) left top no-repeat">
						<a href="/" class="active">
							<p>Автобусы</p>
							<p class="ajandar-price">от 1000 руб/час</p>
						</a>
					</div>					
				</div>
			</div>
			<div class="moduletable">
				<ul id="uslugi">
					<li><a href="/specially.html" class="mainlevel" title="Специальные предложения на аренду автобусов">Спец.предложения</a></li>
					<li><a href="/dissystem.html" class="mainlevel" title="Дисконтная система">Система&nbsp;скидок</a></li>
					<li><a href="/distance.html" class="mainlevel" title="Расчёт загородных поездок">Расчёт&nbsp;поездок</a></li>
					<li><a href="/proverit_zayavku.html" class="mainlevel" title="Проверить заявку">Проверить&nbsp;заявку</a></li>
				</ul>		
			</div>
			<div class="moduletable">
				<h3>Полезная информация</h3>
				<ul class="leftinfo">
					<li><a href="/conditions.html" title="Правила аренды">Условия аренды</a> </li>
					<li><a href="/who-z.html" title="Как арендовать автобус в Санкт-Петербурге?">Как арендовать?</a> </li>

					<li><a href="/faq.html" title="Часто задаваемые вопросы по аренде и заказу автобусов в СПб">Другие вопросы</a></li>
					<li><a href="/glos.html" title="Термины по аренде автобусов">Словарь терминов</a></li>
					<li><a href="/politika-konfidentsialnosti.html" title="Политика конфиденциальности">Политика<br> конфиденциальности</a></li>
				</ul>
			</div>
			<div class="moduletable">
				<h3>Лицензия</h3>
				<img src="/images/liz.gif" alt="Лицензия на перевозку пассажиров" width="125" height="180" border="0">
			</div>
			<div class="moduletable">
				<a href="/korporaty/" style="text-decoration:none">
					<h4 style="background: #eff0f0 url(/images/aja-pidjak.png) 152px 6px no-repeat;">Автобусы для Юридических лиц</h4>
				</a>
				<ul>
					<li style="background:url(/images/aja-galka.png);">VIP-обслуживание корпоративных клиентов</li>
					<li style="background:url(/images/aja-galka.png);">Счет за 20 минут и личный менеджер</li>
					<li style="background:url(/images/aja-galka.png;">Бесплатная доставка закрывающих документов</li>
					<li style="background:url(/images/aja-galka.png;">Бонусные программы и специальные цены</li>				
				</ul>
				<a href="/novosti/strahovanie.html"><strong>Заказать безопасный автобус стало проще!</strong></a>
				<p>Беспокоясь о Вашей безопасности,  мы первыми в Санкт-Петербурге   разработали и подписали Договор о добровольном  страховании пассажиров   от несчастных случаев. <a href="/novosti/strahovanie.html" title="О страхование пассажиров в автобусе">Подробнее о страховании пассажиров </a></p>		
			</div>
			<div class="moduletable">
				<h3>Последние новости</h3>
				<div class="box">
					<div class="box  first box_0">
						<div class="title">
							<a href="/index.php?option=com_content&amp;id=178&amp;task=view">Появление Мерседес Спринтер 515</a>
						</div>
						<p class="text">Очередное пополнение парка компании, совершенно новый Мерседес Спринтер 515 на 19 мест. Оборудован всем...</p>
					</div>
					<div class="box  box_1">
						<div class="title">
							<a href="/price-summer.html">Закажите автобус в аренду на лето по выгодным ценам</a>
						</div>
						<p class="text"></p></div>
						<div class="box  box_2">
							<div class="title">
								<a href="/novosti/gotovte-sani-letom-a-telegu-zimoy.html">Объявляем скидку 10% !</a>
							</div>
							<p class="text"> Внимание! Готовьте сани летом, а телегу зимой! Становитесь участниками новой акции и экономьте при заказе...</p>
						</div>
						<div class="box  box_3">
							<div class="title">
								<a href="/novosti/delano-izmenenie-interera-v-klubnom-avtobuse-patibas.html">Сделано изменение интерьера в клубном автобусе ПатиБас</a>
							</div>
							<div class="text"> Роскошный и вместительный клубный автобус «Party Bus» стал еще комфортнее, благодаря внесенным изменениям в его...</div>
						</div>
					</div>		
			</div>
			<div class="moduletable">
				<a href="/novosti.html">Все новости компании</a>
			</div>
		</div>
	</aside><!-- #secondary -->
<? endif; ?>



