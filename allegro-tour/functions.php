<?php
/**
 * allegro-tour functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package allegro-tour
 */

if ( ! function_exists( 'allegro_tour_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function allegro_tour_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on allegro-tour, use a find and replace
		 * to change 'allegro-tour' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'allegro-tour', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'allegro-tour' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'allegro_tour_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'allegro_tour_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function allegro_tour_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'allegro_tour_content_width', 640 );
}
add_action( 'after_setup_theme', 'allegro_tour_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function allegro_tour_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'allegro-tour' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'allegro-tour' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'allegro_tour_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function allegro_tour_scripts() {
    wp_enqueue_style( 'allegro-tour-fonts', get_template_directory_uri() . '/fonts/fonts.css', array(), '20151215', false );
    wp_enqueue_style( 'allegro-tour-bootstrap-min', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '20151215', false );
	wp_enqueue_style( 'allegro-tour-style', get_stylesheet_uri() );
    wp_enqueue_style( 'allegro-tour-pushy', get_template_directory_uri() . '/css/pushy.css', array(), '20151215', false );
    wp_enqueue_style( 'allegro-tour-jquery-ui-min', get_template_directory_uri() . '/css/jquery-ui.min.css', array(), '20151215', false );
    
    wp_enqueue_style( 'allegro-tour-style-main', get_template_directory_uri() . '/css/style.css', array(), '20151215', false );

	wp_enqueue_script( 'allegro-tour-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'allegro-tour-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_enqueue_script( 'allegro-tour-popper-min-js', get_template_directory_uri() . '/js/popper.min.js', array(), '20151215', true );
	wp_enqueue_script( 'allegro-tour-bootstrap-min-js', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '20151215', true );
	wp_enqueue_script( 'allegro-tour-pushy-min-js', get_template_directory_uri() . '/js/pushy.min.js', array(), '20151215', true );
	wp_enqueue_script( 'allegro-tour-jquery-ui-min-js', get_template_directory_uri() . '/js/jquery-ui.min.js', array(), '20151215', true );
	wp_enqueue_script( 'allegro-tour-sumoselect-min-js', get_template_directory_uri() . '/js/jquery.sumoselect.min.js', array(), '20151215', true );
	wp_enqueue_script( 'allegro-tour-custom-js', get_template_directory_uri() . '/js/custom.js', array(), '20151215', true );
	


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	if ( !is_admin() ) { 
	   wp_deregister_script('jquery-core');
	   wp_register_script('jquery-core', 'https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js', false, false, true);
	   wp_enqueue_script('jquery');
	}
	if( is_front_page() ) {
		wp_enqueue_style( 'allegro-tour-style-slick', get_template_directory_uri() . '/css/slick.css', array(), '20151215', false );
		wp_enqueue_script( 'allegro-tour-slick-js', get_template_directory_uri() . '/js/slick.min.js', array(), '20151215', true );
	}

}
add_action( 'wp_enqueue_scripts', 'allegro_tour_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

function mynew_product_subcategories( $args = array() ) {
	$parentid = get_queried_object_id();
	$args = array(
	    'parent' => $parentid,
	);
	
	$terms = get_terms( 'product_cat', $args );
	if ( $terms ) {   
	    echo '<ul class="product-cats">';
	        foreach ( $terms as $term ) {              
	            echo '<li class="category">';                        
	                woocommerce_subcategory_thumbnail( $term ); 
	                echo '<h2>';
	                    echo '<a href="' .  esc_url( get_term_link( $term ) ) . '" class="' . $term->slug . '">';
	                        echo $term->name;
	                    echo '</a>';
	                echo '</h2>';                                                        
	            echo '</li>';                                                        
	    }
	    echo '</ul>';
	}
}
 
add_action( 'woocommerce_before_shop_loop', 'mynew_product_subcategories', 60 );

function woocommerce_template_loop_category_link_open( $category ) {
	echo esc_url( get_term_link( $category, 'product_cat' ) );
}
function woocommerce_template_loop_category_link_close() {
	echo '';
}
function woocommerce_subcategory_thumbnail( $category ) {	
	$small_thumbnail_size = apply_filters( 'subcategory_archive_thumbnail_size', 'woocommerce_thumbnail' );
	$dimensions           = wc_get_image_size( $small_thumbnail_size );
	$thumbnail_id         = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );

	if ( $thumbnail_id ) {
		$image        = wp_get_attachment_image_src( $thumbnail_id, $small_thumbnail_size );
		$image        = $image[0];
		$image_srcset = function_exists( 'wp_get_attachment_image_srcset' ) ? wp_get_attachment_image_srcset( $thumbnail_id, $small_thumbnail_size ) : false;
		$image_sizes  = function_exists( 'wp_get_attachment_image_sizes' ) ? wp_get_attachment_image_sizes( $thumbnail_id, $small_thumbnail_size ) : false;
	} else {
		if(if_not_cat_img_take_product_img($category->term_id)){
			$image        = if_not_cat_img_take_product_img($category->term_id);
		} else {
			$image        = wc_placeholder_img_src();
		}
		
		$image_srcset = false;
		$image_sizes  = false;
	}

	if ( $image ) {
		// Prevent esc_url from breaking spaces in urls for image embeds.
		// Ref: https://core.trac.wordpress.org/ticket/23605.
		$image = str_replace( ' ', '%20', $image );

		// Add responsive image markup if available.
		if ( $image_srcset && $image_sizes ) {
			echo '<div class="catalog_types--card_img"><img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) .  '" /></div>';
		} else {
			echo '<div class="catalog_types--card_img"><img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '" /></div>';
		}
	}
}
function if_not_cat_img_take_product_img($category_id){
	$args = array( 
	'tax_query' => array( // массив массивов состояящий из:
		array( 
			'taxonomy' => 'product_cat', // название Таксономии
			'field' => 'id', // Производить выбор по 'id' или 'slug' 
			'terms' => $category_id //  ID(ы) или ярлык(и) таксономии, в зависимости от предыдущего параметра
		)), 
		'post_type' => 'product', // Тип записи
		'order' => 'ASC' // порядок сортировки ASC - по возрастанию, DESC - по убыванию(по умолчанию)
	);
	$image = '';
	$loop = new WP_Query( $args );
	$small_thumbnail_size = apply_filters( 'subcategory_archive_thumbnail_size', 'woocommerce_thumbnail' );
	while ( $loop->have_posts() ){
		$loop->the_post();
		global $product;
		if (stripos($product->get_image(), 'заполнитель') != -1){
			$image_id = $product->get_image_id();
			$image = wp_get_attachment_image_src( $image_id,$small_thumbnail_size );
			break;
		}
	}
	return $image[0];
}

function get_filter_classes($category){
	$args = array( 
	'tax_query' => array( // массив массивов состояящий из:
		array( 
			'taxonomy' => 'product_cat', // название Таксономии			
			'field' => 'id', // Производить выбор по 'id' или 'slug'
			'terms' => $category->term_id //  ID(ы) или ярлык(и) таксономии, в зависимости от предыдущего параметра
		)), 
		'post_type' => 'product', // Тип записи
		'posts_per_page' => 100,
		'order' => 'ASC' // порядок сортировки ASC - по возрастанию, DESC - по убыванию(по умолчанию)
	);

	$loop = new WP_Query( $args );
	$classes = array();
	while ( $loop->have_posts() ){
		$loop->the_post();
		global $product;
		$terms = get_the_terms( get_the_ID(), 'product_cat' );		
		
		foreach ($terms as $item){
			if ($item->slug == 'populiarnye' && !in_array('product_cat-populiarnye', $classes)) 
				$classes[] = 'product_cat-populiarnye';
			if ($item->slug == 'novinki' && !in_array('product_cat-novinki', $classes)) 
				$classes[] = 'product_cat-novinki';
			if ($item->slug == 'so_skidkoi' && !in_array('product_cat-so_skidkoi', $classes))
				$classes[] = 'product_cat-so_skidkoi';
		}
	}

	return implode(' ', $classes);
}

function woocommerce_template_loop_category_title( $category ) {
	?>
	<div class="catalog_types--card_title">
		<?php
		echo esc_html( $category->name );

		/*if ( $category->count > 0 ) {
			echo apply_filters( 'woocommerce_subcategory_count_html', ' <mark class="count">(' . esc_html( $category->count ) . ')</mark>', $category ); // WPCS: XSS ok.
		}*/
		?>
	</div>
	<?php
}

function woocommerce_subcategory_price_from($category){
	$price = get_field('cost_from', 'product_cat_'.$category->term_id);
	if ($price)
		echo '<div class="catalog_types--table">
						<span>Стоимость:</span><b>'.$price.'</b>
			</div>';
	elseif(wpp_get_extremes_price_in_product_cat($category->term_id))
		echo '<div class="catalog_types--table">
						<span>Стоимость:</span>
						<b>от '.number_format(wpp_get_extremes_price_in_product_cat($category->term_id), 0, ',', ' ').' рублей</b>
			</div>';
	else
		echo '<table class="catalog_types--table"> </table>';		
}

add_action( 'woocommerce_after_subcategory_title', 'woocommerce_subcategory_price_from', 10 );

function woocommerce_template_loop_product_link_open() {
	global $product;

	$link = apply_filters( 'woocommerce_loop_product_link', get_the_permalink(), $product );

	echo '<a href="' . esc_url( $link ) . '" class="catalog_types--card_body woocommerce-LoopProduct-link woocommerce-loop-product__link">';
}

function woocommerce_template_loop_product_thumbnail() {
	echo'<div class="catalog_types--card_img">';
	echo woocommerce_get_product_thumbnail(); // WPCS: XSS ok.
	echo'</div>';
}

function woocommerce_template_loop_product_title() {
	echo '<div class="catalog_types--card_title">' . get_the_title() . '</div>';
}

function woo_get_region(){
	$parentRegions = 85;
	$regions = [];
	
	$categories = get_the_terms( get_the_ID(), 'product_cat' );
	
	foreach($categories as $cat){
		if ($cat->parent == $parentRegions){
			$regions[] = $cat->name;			
		}
	}
	
	if ($regions){
		echo '<div class="card--place">';
			echo '<div class="ico_pointer"></div>';
			echo '<div class="card--place_text">';
				echo implode(", ", $regions);
			echo '</div>';
		echo '</div>';
	}	
}

add_action( 'woocommerce_shop_loop_item_title', 'woo_get_region', 15 );


function woo_get_short_description(){
	echo '<div class="card--short_description">';
			global $product;
			wc_get_template( 'single-product/product-attributes-custom.php', array(
				'product'            => $product,
				'attributes'         => array_filter( $product->get_attributes(), 'wc_attributes_array_filter_visible' ),
				'display_dimensions' => apply_filters( 'wc_product_enable_dimensions_display', $product->has_weight() || $product->has_dimensions() ),
			) );
	echo '</div>';
}
add_action( 'woocommerce_after_shop_loop_item_title', 'woo_get_short_description', 5 );

function woocommerce_template_loop_add_to_cart( $args = array() ) {}
//function woocommerce_output_related_products() {}
function wpp_get_extremes_price_in_product_cat( $term_id ) {
    
    global $wpdb;
    $sql = "
      SELECT  MIN( meta_value  ) as min_price , MAX( meta_value  ) as max_price
      FROM {$wpdb->posts} 
      INNER JOIN {$wpdb->term_relationships} ON ({$wpdb->posts}.ID = {$wpdb->term_relationships}.object_id)
      INNER JOIN {$wpdb->postmeta} ON ({$wpdb->posts}.ID = {$wpdb->postmeta}.post_id) 
      WHERE  
        ( {$wpdb->term_relationships}.term_taxonomy_id IN (%d) ) 
      AND {$wpdb->posts}.post_type = 'product' 
      AND {$wpdb->posts}.post_status = 'publish' 
      AND {$wpdb->postmeta}.meta_key = '_price'
	  AND meta_value>0
      ";
    
    $result = $wpdb->get_results( $wpdb->prepare( $sql, $term_id ) );
    
    return $result[ 0 ]->min_price;
    
  }
  
  /**
   * Для термина  - product_cat
   */
  add_filter( 'request', 'change_requerst_vars_for_product_cat' );
  add_filter( 'term_link', 'term_link_filter', 10, 3 );
  
  function change_requerst_vars_for_product_cat($vars) {

    global $wpdb;
    if ( ! empty( $vars[ 'pagename' ] ) || ! empty( $vars[ 'category_name' ] ) || ! empty( $vars[ 'name' ] ) || ! empty( $vars[ 'attachment' ] ) ) {
      $slug   = ! empty( $vars[ 'pagename' ] ) ? $vars[ 'pagename' ] : ( ! empty( $vars[ 'name' ] ) ? $vars[ 'name' ] : ( ! empty( $vars[ 'category_name' ] ) ? $vars[ 'category_name' ] : $vars[ 'attachment' ] ) );
      $exists = $wpdb->get_var( $wpdb->prepare( "SELECT t.term_id FROM $wpdb->terms t LEFT JOIN $wpdb->term_taxonomy tt ON tt.term_id = t.term_id WHERE tt.taxonomy = 'product_cat' AND t.slug = %s", array( $slug ) ) );
      if ( $exists ) {
        $old_vars = $vars;
        $vars     = array( 'product_cat' => $slug );
        if ( ! empty( $old_vars[ 'paged' ] ) || ! empty( $old_vars[ 'page' ] ) ) {
          $vars[ 'paged' ] = ! empty( $old_vars[ 'paged' ] ) ? $old_vars[ 'paged' ] : $old_vars[ 'page' ];
        }
        if ( ! empty( $old_vars[ 'orderby' ] ) ) {
          $vars[ 'orderby' ] = $old_vars[ 'orderby' ];
        }
        if ( ! empty( $old_vars[ 'order' ] ) ) {
          $vars[ 'order' ] = $old_vars[ 'order' ];
        }
      }
    }

    return $vars;

  }
  
  function term_link_filter( $url, $term, $taxonomy ) {

    $url = str_replace( "/product_cat/", "/", $url );
    $url = str_replace( "/tipy-ekskursij/", "/", $url );
    return $url;

  }
  
  function get_category_prod($id){
	  global $product;
	  //$cats = $product->get_categories();
	  $cats = wc_get_product_category_list( $id);
	  echo strtok($cats, ',');	
  }

function mayak_nav_menu_no_link($no_link){
$gg_mk = '!<li(.*?)class="(.*?)current-menu-item(.*?)"><a(.*?)>(.*?)</a>!si';
$dd_mk = '<li$1class="\\2current_page_item\\3"><span>$5</span>';
return preg_replace($gg_mk, $dd_mk, $no_link );
}
add_filter('wp_nav_menu', 'mayak_nav_menu_no_link');

add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
 function jk_related_products_args( $args ) {
 
$args['posts_per_page'] = 3; // количество "Похожих товаров"
 $args['columns'] = 3; // количество колонок
 return $args;
}


if ($_SERVER['REQUEST_URI'] == '/bwg_tag/avtobusnye/'||
	$_SERVER['REQUEST_URI'] == '/bwg_tag/interesnye/'||
	$_SERVER['REQUEST_URI'] == '/bwg_tag/neobychnye-muzei/'||
	$_SERVER['REQUEST_URI'] == '/bwg_gallery/galereya/'||
	$_SERVER['REQUEST_URI'] == '/category/bez-rubriki/'	) {
    status_header( 404 );
    nocache_headers();
    include( get_query_template( '404' ) );
    die();
}

// Add TITLE for "Add to wishlist" button
add_filter( 'tinvwl_wishlist_button', 'add_title_to_the_button', 99 , 7 );
function add_title_to_the_button( $content, $wishlist, $product, $is_loop, $icon, $action, $text ) {
	
	if( false === strpos( $content, 'tinvwl-product-in-list' ) ) {
		
		$title_text = 'Добавить экскурсию в закладки';
		
	} else {
		
		$title_text = 'Экскурсия уже в закладках';
		
	}
	if( $edited_content = str_replace( '<a class', '<a title="' . $title_text . '" class', $content, $count ) ) {
		
		return $edited_content;
		
	} else {
		
		return $content;
		
	}
	
	return $content;
	
}

/**
 * Добавляем .html в конец URL постоянных страниц
 * Чтобы код начал работать вам нужно сбросить правила ЧПУ, для этого
 * зайдите в настройки ЧПУ и просто нажмите кнопку СОХРАНИТЬ.
 */

// если у вас в ЧПУ не указан слэш на конце, то этот фильтр можно отключить,
// для этого закомментируйте следующую строку.
add_filter('user_trailingslashit', 'noPage_slash', 66, 2 );
function noPage_slash( $string, $type ){
	global $wp_rewrite;

	if( $wp_rewrite->using_permalinks() && $wp_rewrite->use_trailing_slashes == true && $type == 'page'){
		return untrailingslashit( $string );
	}
	return $string;
}

// добавляем '.html' в структуру ЧПУ для page типа
add_action('init', 'htmlPage_permalink', -1);
function htmlPage_permalink() {
	global $wp_rewrite;

	 if( ! strpos( $wp_rewrite->get_page_permastruct(), '.html') ){
		$wp_rewrite->page_structure = $wp_rewrite->page_structure . '.html';
	 }
}

//вывод списка всех записей сайта с помощью шорткода start
function wph_allposts_shortcode($atts, $content) {
 extract( shortcode_atts( array(
        'cat' => ''
    ), $atts ) );
    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page'   => -1,
        'orderby' => 'title',
        'order' => 'ASC',
		'cat'        => $atts['cat'] 
    ); //print_r($args);
    $query = new WP_Query( $args );

    if( $query->have_posts() ) {
        echo '<ul class="allposts">';
        while ( $query->have_posts() ) {
            $query->the_post();
            echo '<li><a href="'.get_the_permalink().'">' . 
            get_the_title() . '</a></li>';
        }
        echo '</ul>';
    }
 
    wp_reset_postdata();
}
add_shortcode('allposts', 'wph_allposts_shortcode');
//вывод списка всех записей сайта с помощью шорткода end


//remove_filter( 'pre_term_description', 'wp_filter_kses' );
//remove_filter( 'term_description', 'wp_kses_data' );
 
function mayak_category_description($container = ''){
    $content = is_object($container) && isset($container->description) ? html_entity_decode($container->description) : '';
    $editor_id = 'tag_description';
    $settings = 'description';     
    ?>
    <tr class="form-field">
    <th scope="row" valign="top"><label for="description">Описание</label></th>
    <td><?php wp_editor($content, $editor_id, array(
                'textarea_name' => $settings,
                'editor_css' => '<style> .html-active .wp-editor-area{border:0;}</style>',
    )); ?><br />
    <span class="description">Описание по умолчанию не отображается, однако некоторые темы могут его показывать!.</span></td>
    </tr>
    <?php   
}
//add_filter('edit_category_form_fields', 'mayak_category_description');
//add_filter('edit_tag_form_fields', 'mayak_category_description');




function hidden_term_description() {
	print '<style>
	.term-description-wrap { display:none; }
	</style>';
} 
add_action('admin_head', 'hidden_term_description');



function cptui_register_my_cpts_reviews() {

	/**
	 * Post Type: Отзывы.
	 */

	$labels = array(
		"name" => __( "Отзывы", "allegro" ),
		"singular_name" => __( "Отзыв", "allegro" ),
		"menu_name" => __( "Отзывы", "allegro" ),
		"all_items" => __( "Все отзывы", "allegro" ),
		"add_new" => __( "Добавить отзыв", "allegro" ),
		"add_new_item" => __( "Добавить новый отзыв", "allegro" ),
		"edit_item" => __( "Редактировать отзыв", "allegro" ),
		"new_item" => __( "Новый отзыв", "allegro" ),
		"view_item" => __( "Смотреть отзыв", "allegro" ),
		"view_items" => __( "Смотреть отзывы", "allegro" ),
		"search_items" => __( "Найти отзыв", "allegro" ),
		"not_found" => __( "Отзывы не найдены", "allegro" ),
		"not_found_in_trash" => __( "Отзывы не найдены в корзине", "allegro" ),
		"parent_item_colon" => __( "Родительский отзыв", "allegro" ),
		"featured_image" => __( "Изображение", "allegro" ),
		"set_featured_image" => __( "Установить изображение", "allegro" ),
		"remove_featured_image" => __( "Удалить изображение", "allegro" ),
		"use_featured_image" => __( "Использовать как изображение к отзыву", "allegro" ),
		"archives" => __( "Архив отзывов", "allegro" ),
		"insert_into_item" => __( "Вставить в отзыв", "allegro" ),
		"uploaded_to_this_item" => __( "Загружено к этому отзыву", "allegro" ),
		"filter_items_list" => __( "Фильтровать список отзывов", "allegro" ),
		"items_list_navigation" => __( "Навигация по списку отзывов", "allegro" ),
		"items_list" => __( "Список отзывов", "allegro" ),
		"attributes" => __( "Атрибуты отзыва", "allegro" ),
		"name_admin_bar" => __( "Отзыв", "allegro" ),
		"parent_item_colon" => __( "Родительский отзыв", "allegro" ),
	);

	$args = array(
		"label" => __( "Отзывы", "allegro" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "reviews", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor" ),
	);

	register_post_type( "reviews", $args );
}

add_action( 'init', 'cptui_register_my_cpts_reviews' );
