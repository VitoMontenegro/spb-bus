<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package allegro-tour
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<?php wp_head(); ?>

<!-- Google Tag Manager --><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:''; j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-K4VW925');</script><!-- End Google Tag Manager -->		
	</head>

	<body <?php body_class(); ?>>
		<!-- Google Tag Manager (noscript) --><noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K4VW925" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><!-- End Google Tag Manager (noscript) -->
		
		<section class="head container">
			<div class="row">
				<div class="head--logo col-lg-3 col-md-6 col-12">
					<? if($_SERVER['REQUEST_URI'] == '/') {?>
						<img src="<?php bloginfo('template_directory');?>/img/logo-lite.png">
					
					<? }else{  ?>
					<a href="/"><img src="<?php bloginfo('template_directory');?>/img/logo-lite.png"></a>
					<? } ?>
				</div>
				<div class="head--logo col-lg-4 col-md-6 col-12">
					<div class="slogan">
			          Аренда автобусов с водителем <br> в Санкт-Петербурге
			        </div>
				</div>
				<div class="head--buttons col-lg-2 col-12">
					<div class="row">
							<!--<a href="#" class="head--button head--call" data-toggle="modal" data-target="#call">Заказать звонок</a>
							<a href="#" class="head--button head--letter" data-toggle="modal" data-target="#message">Написать письмо</a>-->
						
						<div class="col-12 col-lg-12 col-sm-12 col-md-12">
							<a href="#" class="head--button head--rent" data-toggle="modal" data-target="#order">Забронировать</a>
							<a href="#" class="head--button head--status" data-toggle="modal" data-target="#status">Статус заказа</a>
						</div>
					</div>
				</div>
				<div class="head--phones col-lg-3 col-md-6 col-12">
					
					<a href="tel:8127156464"><div class="ico_phone_icon ico_phone_icon_small"></div> (812) 715-64-64</a>

					
					<a href="viber://add?number=8129860150"><div class="ico_phone_icon ico_phone_icon_small"></div> (812) 986-01-50</a>
					<!--a class="head--tel-icons" href="whatsapp://send?phone=+79262536656"><div class="whatsup"></div>79262536656</a-->
					<a href="#" class="head--call--phone" data-toggle="modal" data-target="#call">обратный звонок</a>
				</div>
			</div>
			
			
			
			<div id="container" class="head--mobile_button">
				<button class="menu-btn"><img src="<?php bloginfo('template_directory');?>/img/burger-black.png"></button>
			</div>
			
			<nav class="pushy pushy-right">
				<div class="pushy-content">
					<ul>
						<!-- Submenu -->
						<li class="pushy-submenu submenu--classes">
							<button>Классы</button>
							<?php wp_nav_menu(array('menu' => 'sidebar_klassy','container' => 'ul', 'menu_class' => 'menu1')); ?>
						</li>
						<li class="pushy-submenu submenu--predmet">
							<button>Предметы</button>
							<?php wp_nav_menu(array('menu' => 'sidebar_predmety','container' => 'ul', 'menu_class' => 'menu1')); ?>
						</li>
						<li class="pushy-submenu submenu--region">
							<button>Регионы</button>
							<?php wp_nav_menu(array('menu' => 'sidebar_regiony','container' => 'ul', 'menu_class' => 'menu1')); ?>
						</li>
					</ul>
				</div>
			</nav>
			<div class="site-overlay"></div>

		</section>

		<section class="container-fluid menu">
			<?php wp_nav_menu(array(
                'theme_location'    => 'primary',
                'menu' =>'Menu 1'
                ));
            ?>
		</section>

