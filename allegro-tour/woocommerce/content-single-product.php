<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div class="breadcrumbs">
	<a href="/">Автопарк</a> — 
	<?php get_category_prod(get_the_ID()); ?> — 
	<span><?php the_title();?></span>
</div>
<h1><?php if (get_field('h1')) echo get_field('h1'); else the_title();?></h1>

<div id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>
<?php /*echo do_shortcode( '[ti_wishlists_addtowishlist]' );*/ ?>
<?php /* echo '<div class="print-icon"><a href="javascript:(print());"><img src="https://spb-bus.ru/wp-content/uploads/2019/01/print.png" alt="распечатать экскурсию" title="распечатать экскурсию"></a></div>'; */ ?>
	<?php
		/**
		 * Hook: woocommerce_before_single_product_summary.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="summary entry-summary">
		<?php
			/**
			 * Hook: woocommerce_single_product_summary.
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 * @hooked WC_Structured_Data::generate_product_data() - 60
			 */

			remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
			remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
			remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
			remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
			remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
			remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
			
			add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 5 );
			add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 10 );
			add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 15 );
			do_action( 'woocommerce_single_product_summary' );
		?>

	    <?php 
	    $product_id = get_the_ID(); 
	    $current_season_id = luxrent_rental_get_current_period();
		$luxrent_prices = get_post_meta( $product_id, 'luxrent_billing_' . $current_season_id, true ); 
		?>

		<?php if ( luxrent_rental_prices_are_filled( $luxrent_prices ) ) : ?>
			<?php $prepared_prices = luxrent_rental_frontend_prices( $luxrent_prices ); ?>
			<table class="shop_attributes">
				<tbody>	
				<?php 
					$wdiap=[]; //day
					$wprice=[];  //price


					if (!empty($prepared_prices["ПН"])) {
						$wdiap[] = 'ПН'; 
						$wprice[]=$prepared_prices["ПН"]["regular_price"];
					}

					if (!empty($prepared_prices["ЧТ"])) {
						$wdiap[] = 'ЧТ'; 

						if ($prepared_prices["ПН"]["regular_price"] != $prepared_prices["ЧТ"]["regular_price"]) {
							$wprice[] = $prepared_prices["ЧТ"]["regular_price"];
						}
					}
				?>
				<?php if (!empty($wdiap) && !empty($wprice)) : ?>
					<tr>
						<th>ПН-ЧТ</th>
						<td><p><?php echo implode(",",$wprice); ?><small> руб./час</small></p></td>
					</tr>
				<?php endif; ?>

				<?php 
					$wdiap=[]; //day
					$wprice=[];  //price


					if (!empty($prepared_prices["ПТ"])) {
						$wdiap[] = 'ПТ'; 
						$wprice[]=$prepared_prices["ПТ"]["regular_price"];
					}

					if (!empty($prepared_prices["СБ"])) {
						$wdiap[] = 'СБ'; 

						if ($prepared_prices["ПТ"]["regular_price"] != $prepared_prices["СБ"]["regular_price"]) {
							$wprice[] = $prepared_prices["СБ"]["regular_price"];
						}
					}
				?>
				<?php if (!empty($wdiap) && !empty($wprice)) : ?>
					<tr>
						<th><?php echo implode(",",$wdiap); ?></th>
						<td><p><?php echo implode(",",$wprice); ?><small> руб./час</small></p></td>
					</tr>
				<?php endif; ?>

				<?php if (!empty($prepared_prices["ВС"])) : ?>	
					<tr>
						<th>ВС</th>		
						<td><p><?php echo $prepared_prices["ВС"]["regular_price"];  ?><small> руб./час</small></p></td>
					</tr>
				<?php endif; ?>	
				<?php if (!empty($prepared_prices["СВАДЬБА"])) : ?>	
					<tr>
						<th>Свадьба</th>		
						<td><p><?php echo $prepared_prices["СВАДЬБА"]["regular_price"];  ?><small> руб./час</small></p></td>
					</tr>
				<?php endif; ?>	

				<?php if (!empty($prepared_prices["ТРАНСФЕР"])) : ?>	
					<tr>
						<th>Трансфер</th>		
						<td><p><?php echo $prepared_prices["ТРАНСФЕР"]["regular_price"];  ?><small> руб.</small></p></td>
					</tr>
				<?php endif; ?>		
				<?php if (!empty($prepared_prices["СПЕЦ.ЦЕНА"])) : ?>	
					<tr>
						<th>Спец.цена*</th>		
						<td><p><?php echo $prepared_prices["СПЕЦ.ЦЕНА"]["regular_price"];  ?><small> руб./час</small></p></td>
					</tr>
				<?php endif; ?>	


				

				</tbody>
			</table>
			<?php if (!empty($prepared_prices["СПЕЦ.ЦЕНА"])) : ?>	
				<p><small>* Спец. цена - для постоянных клиентов, туристических, event - компаний,администраций и организаций сферы образования.</small></p>
			<?php endif; ?>	
		<?php endif ?>

		
		<div class="single_product--btns clearfix">
			<?php /* <a href="#" class="head--button head--letter" data-toggle="modal" data-target="#message">Написать письмо</a> */ ?>
			<a href="#" class="head--button head--rent" data-toggle="modal" data-target="#order">Забронировать</a>
		</div>
		<div class="social-btn">
	      <a href="http://vkontakte.ru/share.php?url=<?php echo "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>" target="_blank" title="Vkontakte"><i class="fab fa-facebook-f"></i><img src="<?php echo get_site_url();?>/wp-content/uploads/2019/01/004-vk.png"></a>
	      <a href="https://www.facebook.com/sharer.php?u=<?php echo "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>" target="_blank" title="Facebook"><img src="<?php echo get_site_url();?>/wp-content/uploads/2019/01/003-facebook.png"></a>
	      <a href="http://twitter.com/share?text=<?php echo "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>" target="_blank"  title="twitter"><img src="<?php echo get_site_url();?>/wp-content/uploads/2019/01/002-twitter.png"></a>
	      <a href="https://wa.me/?text=<?php echo "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>" target="_blank"  title="Whatsapp"><img src="<?php echo get_site_url();?>/wp-content/uploads/2019/01/WhatsApp.png"></a>
	      <a href="https://telegram.me/share/url?url=<?php echo "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>" target="_blank"  title="Telegram"><img src="<?php echo get_site_url();?>/wp-content/uploads/2019/01/telegram-app.png"></a>
	      <a href="https://connect.ok.ru/offer?url=<?php echo "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>" target="_blank"  title="Odnoklassniki"><img src="<?php echo get_site_url();?>/wp-content/uploads/2019/01/logo_odnoklassniki.png"></a>
	    </div>
	</div>

	<?php
		/**
		 * Hook: woocommerce_after_single_product_summary.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
	?>

</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
