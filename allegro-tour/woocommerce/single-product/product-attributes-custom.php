<?php
/**
 * Product attributes
 *
 * Used by list_attributes() in the products class.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-attributes.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<table class="shop_attributes">
	<?php if ( $display_dimensions && $product->has_weight() ) : ?>
		<tr>
			<th><?php _e( 'Weight', 'woocommerce' ) ?></th>
			<td class="product_weight"><?php echo esc_html( wc_format_weight( $product->get_weight() ) ); ?></td>
		</tr>
	<?php endif; ?>

	<?php if ( $display_dimensions && $product->has_dimensions() ) : ?>
		<tr>
			<th><?php _e( 'Dimensions', 'woocommerce' ) ?></th>
			<td class="product_dimensions"><?php echo esc_html( wc_format_dimensions( $product->get_dimensions( false ) ) ); ?></td>
		</tr>
	<?php endif; ?>





<?php

$product_id = $product->get_id();

$current_season_id = luxrent_rental_get_current_period();
$luxrent_prices = get_post_meta( $product_id, 'luxrent_billing_' . $current_season_id, true );

if ( luxrent_rental_prices_are_filled( $luxrent_prices ) ) : ?>

	<?php $prepared_prices = luxrent_rental_frontend_prices( $luxrent_prices ); //print_r($prepared_prices);?>

	<tr>
		<th>Спец.цена*</th>
		<?php if (!empty($prepared_prices["СПЕЦ.ЦЕНА"])) : ?>		
			<td><p><?php echo $prepared_prices["СПЕЦ.ЦЕНА"]["regular_price"];  ?><small> руб./час</small></p></td>
		<?php else: ?>
			<td>-</td>			
		<?php endif; ?>
	</tr>
	<tr>
		<th>Трансфер*</th>
		<?php if (!empty($prepared_prices["ТРАНСФЕР"])) : ?>		
			<td><p><?php echo $prepared_prices["ТРАНСФЕР"]["regular_price"];  ?><small> руб.</small></p></td>
		<?php else: ?>
			<td>-</td>			
		<?php endif; ?>
	</tr>
	<?php foreach ( $attributes as $attribute ) : ?>
		<tr>
			<th><?php echo wc_attribute_label( $attribute->get_name() ); ?></th>
			<td><?php
				$values = array();

				if ( $attribute->is_taxonomy() ) {
					$attribute_taxonomy = $attribute->get_taxonomy_object();
					$attribute_values = wc_get_product_terms( $product->get_id(), $attribute->get_name(), array( 'fields' => 'all' ) );

					foreach ( $attribute_values as $attribute_value ) {
						$value_name = esc_html( $attribute_value->name );

						if ( $attribute_taxonomy->attribute_public ) {
							$values[] = '<a href="' . esc_url( get_term_link( $attribute_value->term_id, $attribute->get_name() ) ) . '" rel="tag">' . $value_name . '</a>';
						} else {
							$values[] = $value_name;
						}
					}
				} else {
					$values = $attribute->get_options();

					foreach ( $values as &$value ) {
						$value = make_clickable( esc_html( $value ) );
					}
				}

				echo apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );
			?></td>
		</tr>
	<?php endforeach; ?>
	<tr>
		<th>Цена*</th>
		<?php if (!empty($prepared_prices["min"])) : ?>		
			<td><p>от <?php echo $prepared_prices["min"];  ?><small> руб./час</small></p></td>
		<?php else: ?>
			<td>-</td>			
		<?php endif; ?>
	</tr>
<?php $wdiap=array(); $wprice=array(); //print_r($prepared_prices);
if (!empty($prepared_prices["ПТ"])) {$wdiap[]='ПТ'; $wprice[]=$prepared_prices["ПТ"]["regular_price"];}
if (!empty($prepared_prices["СБ"])) {
	$wdiap[]='СБ'; 
	if ($prepared_prices["ПТ"]["regular_price"]!=$prepared_prices["СБ"]["regular_price"])
		$wprice[]=$prepared_prices["СБ"]["regular_price"];
	}
?>
	<tr>
		<th><?php if (!empty($wdiap)) : ?><?php echo implode(",",$wdiap); ?><?php endif; ?></th>
		<?php if (!empty($wdiap) && !empty($wprice)) : ?>		
			<td><p><?php echo implode(",",$wprice); ?><small> руб./час</small></p></td>
		<?php else: ?>
			<td>-</td>			
		<?php endif; ?>
	</tr>



<?php endif; ?>

</table>