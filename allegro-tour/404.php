<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package allegro-tour
 */

get_header();
?>

<section class="container">
	<div class="row">
		<h1>ЗАПРОШЕННАЯ ВАМИ СТРАНИЦА НЕ НАЙДЕНА</h1>
		<div class="img_404">
			<img src="/wp-content/themes/allegro-tour/img/404_error.jpg" alt="">
		</div>
		<div class="block_404">
			<p id="home">Главная страница <a href="/">SPB-BUS.RU</a> откроется вместо этой через 5 секунд.</p>
		</div>
	</div>
</section>
<script>
  var t=5;
    setInterval(home,1000);
    function home(){
        if(t==0){
            location="https://spb-bus.ru";
        }
        if(t==5)
            document.getElementById('home').innerHTML='Главная страница <a href="/">spb-bus.ru</a> откроется вместо этой через 5 секунд.';
        if(t==4)
            document.getElementById('home').innerHTML='Главная страница <a href="/">spb-bus.ru</a> откроется вместо этой через 4 секунд.';
        if(t==3)
            document.getElementById('home').innerHTML='Главная страница <a href="/">spb-bus.ru</a> откроется вместо этой через 3 секунд.';
        if(t==2)
            document.getElementById('home').innerHTML='Главная страница <a href="/">spb-bus.ru</a> откроется вместо этой через 2 секунд.';
        if(t==1)
            document.getElementById('home').innerHTML='Главная страница <a href="/">spb-bus.ru</a> откроется вместо этой через 1 секунд.';

        t--;
    }
</script>
<style>
	.img_404 img{
		max-width: 300px;
	}
	.img_404{
		margin:0 10px 10px 0
	}
</style>
<?php
get_footer();
