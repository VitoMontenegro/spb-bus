$(document).ready(function(){
	$("#menu-sidebar_klassy li").clone().appendTo(".submenu--classes ul");
	$("#menu-sidebar_predmety li").clone().appendTo(".submenu--predmet ul");
	$("#menu-sidebar_regiony li").clone().appendTo(".submenu--region ul");
	$("section.menu > div ul li").clone().appendTo(".pushy-content > ul");
	window.asd = $('.SlectBox').SumoSelect({ csvDispCount: 3, selectAll:true, captionFormatAllSelected: "Yeah, OK, so everything." });
	$(window).scroll(function() {
		var scrollTop = $(document).scrollTop()*1;
		
		if (scrollTop > 300){
			$('section.menu').addClass('fixed');
			$('section.head').addClass('head--custom_margin');
			if ($('body').hasClass("admin-bar")){
				$('section.menu').addClass('fixed-admin');
			}
		} else {
			$('section.menu').removeClass('fixed');
			$('section.menu').removeClass('fixed-admin');
			$('section.head').removeClass('head--custom_margin');
		}
	});
	
	$( "#slider-range" ).slider({
      range: true,
      min: 1,
      max: 11,
      values: [ 1, 11 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( + ui.values[ 0 ] + " - " + ui.values[ 1 ] + " классы" );
		$('#klass_from').val(ui.values[ 0 ]);
		$('#klass_to').val(ui.values[ 1 ]);		
      },
	   change: function( event, ui ) {
		filterCard();
	   }
    });
    $( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) +
      " - " + $( "#slider-range" ).slider( "values", 1 ) + " классы" );
	  
	filterCard();
	$('.form--filter').change(function(){filterCard();});
	
	$(this).find('[name=_urkl]').val(window.location.href);
});

function filterCard(){
	var novinki = 0,
		so_skidkoi = 0,
		populiarnye = 0;
		
	var klass_from = $('#klass_from').val()*1,
		klass_to = $('#klass_to').val()*1;
		
	$('.catalog_types--card').show();
	
	$('.card--item_filter input:checked').each(function(){
		$('.catalog_types--card:not(.'+$(this).val()+')').hide();
	});	
	
	if (klass_from > 1 || klass_to < 11){
		$('.catalog_types--card').removeClass('hasKlass');
		for (i = klass_from; i < klass_to+1; i++) {
			$('.catalog_types--card').each(function(){				
				if ($(this).hasClass('product_cat-'+i+'-klass')) $(this).addClass('hasKlass');
			});					
		}
		$('.catalog_types--card:not(.hasKlass)').hide();	
	}
	
	$('.card--item_filter input[type="checkbox"]').each(function(){
		var value = $(this).val();
		var count = 0;
		$('.catalog_types--card').each(function(){
			if ($(this).hasClass(value) && $(this).is(':visible')){
				count++;
			}
		});
		
		if (count == 0){
			$(this).parent().addClass('not_have_res').hide();
		} else {
			$(this).parent().removeClass('not_have_res').show();
			$(this).parent().find('label span').text('('+count+')');
		}
	});
	
	var checkKlasses = false;
	for (i = klass_from; i < klass_to+1; i++) {
		$('.catalog_types--card').each(function(){				
			if ($(this).hasClass('product_cat-'+i+'-klass') && $(this).is(':visible')) checkKlasses = true;
		});					
	}
	if (checkKlasses == false) $('.card--item_filter-range').addClass('not_have_res');
	else $('.card--item_filter-range').removeClass('not_have_res');
	
	
	$('.card').each(function(){
		var show = false;
		$(this).show();
		$(this).find('.card--item_filter').each(function(){
			if (!$(this).hasClass('not_have_res')) show = true;
		});
		
		if (show == false) $(this).hide();
	});
	
	if(!$('.catalog_types--card:visible').length){
		$('.card').show();
	}
	
	$(this).click(function(){greatBalancer(".catalog_types--card",".catalog_types--card_title",".catalog_types--table", ".card--short_description");});
	greatBalancer(".catalog_types--card",".catalog_types--card_title",".catalog_types--table", ".card--short_description");
}

function greatBalancer(block){
	var wrapWidth = $(block).parent().width(),  // 1
		blockWidth = $(block).width(),          // 2
		wrapDivide = Math.floor(wrapWidth / blockWidth),     // 3
		cellArr = $(block);
		
	for(var arg = 1;arg<=arguments.length;arg++) {           // 4.1
		for (var i = 0; i <= cellArr.length; i = i + wrapDivide) {
			var maxHeight = 0,
				heightArr = [];
				
			for (j = 0; j < wrapDivide; j++) {               // 4.2
				heightArr.push($(cellArr[i + j]).find(arguments[arg]));
				
				if (heightArr[j].outerHeight() > maxHeight) {
					maxHeight = heightArr[j].outerHeight();
				}
			}
			
			for (var counter = 0; counter < heightArr.length; counter++) {           // 4.3
				$(cellArr[i + counter]).find(arguments[arg]).outerHeight(maxHeight);
			}
		}
    }
}