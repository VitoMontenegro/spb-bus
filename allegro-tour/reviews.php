<?php
/**
 * Страница с кастомным шаблоном (page-custom.php)
 * @package WordPress
 * Template Name: Отзывы
 */
get_header(); ?>


        <section class="container">
            <div class="row">
                <?php get_sidebar(); ?>
                <div class="content--body col-lg-9">
                    <div class="shadowed default-floated block">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/preloader.svg" class="preloader">
                    <?php
                        $args = array(
                            'posts_per_page' => '999',
                            'post_type' => 'reviews'
                        );
                        $myposts = get_posts( $args );
                        foreach( $myposts as $post ){ setup_postdata($post);
                        ?>
                            <div class="review-item">
                                <h3><?php the_title(); ?></h3>
                                <div class="review-content">
                                    <?php the_content(); ?>
                                    <div class="review-content-img">
                                        <?php 
                                            if (get_post_meta( $post->ID, 'review_img', true )) {
                                                $img_id = get_post_meta( $post->ID, 'review_img', true );
                                                echo wp_get_attachment_image( $img_id, 'medium');
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                    wp_reset_postdata(); // сбрасываем переменную $post
                    ?>
                     <div class="show-more-reviews">Показать еще</div>
                    </div>
                    <div class="shadowed default-floated block">
                        <div class="add-review-form">
                            <h2>Оставить отзыв</h2>
                            <?php echo do_shortcode('[contact-form-7 id="1749" title="Отзывы"]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<style>
.review-item {
    border-bottom: 1px dashed #e2e2e2;
    padding: 0 0 20px 0;
    display: none;
}
.show-more-reviews {
    color: #0fb3f1;
    font-weight: 600;
    text-align: center;
    text-decoration: underline;
    text-decoration-style: dashed;
    text-underline-position: under;
    padding: 0 0 2px 0;
    margin: 20px auto 0 auto;
    display: none;
    box-sizing: content-box;
    cursor: pointer;
}
</style>
<script>
(function($) {
    $(document).ready(function() {
        $('.review-item:lt(10)').show();
        $('.preloader').hide();
        $('.show-more-reviews').show();
        $('.add-review-form').show();
        $('.add-review-form .wpuf-insert-image').text('Добавить фото');
        $('.reviews-filter-item').on('click', function() {
            $('.reviews-filter-item').removeClass('active');
            $(this).addClass('active');
        });
        $('.all-reviews').on('click', function() {
            $('.review-item').show();
            $('.review-item').animate({opacity: 0}, 0).animate({opacity: 1}, 800);
            $('.show-more-reviews').hide();
        });
        $('.voice-reviews').on('click', function() {
            $('.review-item').hide();
            $('.wp-audio-shortcode').closest('.review-item').show();
            $('.wp-audio-shortcode').closest('.review-item').animate({opacity: 0}, 0).animate({opacity: 1}, 800);
            $('.show-more-reviews').hide();
        });
        $('.text-reviews').on('click', function() {
            $('.review-item').show();
            $('.wp-audio-shortcode').closest('.review-item').hide();
            $('.review-item').animate({opacity: 0}, 0).animate({opacity: 1}, 800);
            $('.show-more-reviews').hide();
        });
        $('.show-more-reviews').on('click', function() {
            $('.review-item:hidden:lt(10)').show();
        });
        $('.add-review-btn').on('click', function() {
            $('html, body').animate({
                scrollTop: $(".add-review-form").offset().top - 100
            }, 800);
        });
    });
})( jQuery );
</script>

<?php get_footer(); ?>