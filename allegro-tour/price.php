<?php
/**
 * Страница с кастомным шаблоном (page-custom.php)
 * @package WordPress
 * Template Name: Прайс
 */
get_header(); ?>


        <section class="container">
            <div class="row">

                <?php get_sidebar(); ?>

                <div class="content--body col-lg-9">

                    <div class="breadcrumbs">
                        <a href="/">Автопарк</a> — 
                        <span><?php the_title();?></span>
                    </div>

                    <h1><?php if (get_field('h1')) echo get_field('h1'); else the_title();?></h1>  
                    <p class="price-link">
                        <a href="<?php the_field('price_list'); ?>">  <?php the_field('name_price'); ?>  </a>
                    </p>                 
                    <?php the_content(); ?>
                </div>
            </div>
        </section>

<?php get_footer(); ?>