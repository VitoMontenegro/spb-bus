		<section class="container-fluid footer">
			<div class="container">
				<div class="row ">
 					<div class="footer--column col-lg-3 col-md-6 col-sm-6">
						<div class="footer--column-title">Меню</div>
				  		<?php wp_nav_menu(array('menu' => 'menu_footer-2','container' => 'ul', 'menu_class' => 'menu-footer')); ?>
					</div>

					<div class="footer--column col-lg-5 col-md-6 col-sm-6">
						<div class="footer--column-title text-center">Полезная информация</div>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12">
														  		<?php wp_nav_menu(array('menu' => 'footer_profit_1','container' => 'ul', 'menu_class' => 'menu-footer')); ?>
						  		<?php /*
								<a href="#" class="head--call" data-toggle="modal" data-target="#call">Заказать звонок</a>
								<a href="#" class="head--rent" data-toggle="modal" data-target="#order">Забронировать</a>
								<a href="#" class="head--status" data-toggle="modal" data-target="#status">Статус заказа</a>
								<a href="#" class="head--letter" data-toggle="modal" data-target="#message">Написать письмо</a>
								*/ ?>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12">
														  		<?php wp_nav_menu(array('menu' => 'footer_profit_2','container' => 'ul', 'menu_class' => 'menu-footer')); ?>
						  		<?php /*
								<a href="#" class="head--call" data-toggle="modal" data-target="#call">Заказать звонок</a>
								<a href="#" class="head--rent" data-toggle="modal" data-target="#order">Забронировать</a>
								<a href="#" class="head--status" data-toggle="modal" data-target="#status">Статус заказа</a>
								<a href="#" class="head--letter" data-toggle="modal" data-target="#message">Написать письмо</a>
								*/ ?>
							</div>
						</div>
					</div>

					<div class="footer--column footer--contacts col-lg-4 col-md-6">
						<div class="footer--column-title">Контакты</div>
						191014, Санкт-Петербург, пр. Королева д. 7						 
						<br>
						Тел./факс: <a class="text-decor--none" href="tel:+78127156464">+7 (812) 715-64-64</a><a href="#" class="head--call order--call" data-toggle="modal" data-target="#call" style="display:inline"> обратный звонок</a>
						<br>
						<a class="footer--tel-icons text-decor--none" href="viber://add?number=+78127156464">
							<span class="wviber"></span>78127156464
						</a>
						<a class="footer--tel-icons text-decor--none" href="whatsapp://send?phone=+78129860150">
							<span class="wwhatsup"></span>78129860150
						</a>
						<div class="footer--email">
							E-mail: <a class="text-decor--none" href="mailto:7156464@mail.ru">7156464@mail.ru </a>
							<a href="#" class="popup" data-toggle="modal" data-target="#send_ceo">Написать директору</a>
						</div>
						<p>
							<img alt="" class="img_doc lazyload" src="<?php bloginfo('template_directory');?>/img/document-word-text.png">
							<a href="/wp-content/uploads/2018/07/svidetelstvo.pdf" target="_blank">Свидетельство о регистрации</a>
						</p>
					</div>
				</div>
			</div>
			<div class="footer--copyright">© 2004 — <?=date("o")?></div>
		</section>
		
		<div class="modal modal--custom" id="call" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-dialog-centered bd-example-modal-sm" role="document">
			<div class="modal-content">			 
			  <a title="Close" class="modal--close" href="javascript:;" data-dismiss="modal" aria-label="Close"></a>
			  <div class="modal-body">
				<?php echo do_shortcode( '[contact-form-7 id="1502" title="Заказать звонок"]' ); ?>
			  </div>
			</div>
		  </div>
		</div>
		
		<div class="modal modal--custom" id="message" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-dialog-centered bd-example-modal-sm" role="document">
			<div class="modal-content">			 
			  <a title="Close" class="modal--close" href="javascript:;" data-dismiss="modal" aria-label="Close"></a>
			  <div class="modal-body">
				<?php echo do_shortcode( '[contact-form-7 id="98" title="Написать письмо"]' ); ?>
			  </div>
			</div>
		  </div>
		</div>
		
		<div class="modal modal--custom" id="order" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-dialog-centered bd-example-modal-sm" role="document">
			<div class="modal-content">			 
			  <a title="Close" class="modal--close" href="javascript:;" data-dismiss="modal" aria-label="Close"></a>
			  <div class="modal-body">
				<?php if ( function_exists( 'is_product' ) ) {
					if( is_product() ) {
						echo '<h5>' . get_the_title() . '</h5>';
					}					
				} ?>
				<?php echo do_shortcode( '[contact-form-7 id="1501" title="Забронировать"]' ); ?>
			  </div>
			</div>
		  </div>
		</div>
		
		<div class="modal modal--custom" id="status" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-dialog-centered bd-example-modal-sm" role="document">
			<div class="modal-content">			 
			  <a title="Close" class="modal--close" href="javascript:;" data-dismiss="modal" aria-label="Close"></a>
			  <div class="modal-body">
				<form class="status_order" method="post" onsubmit="return get_info_status();">
					<p>
					<label style="width: 100%;">
						Номер заявки:
						<input type="text" id="the_order_id" name="stat_num">
					</label>
					</p>
					<div id="result-status" class="response hidden"></div>
					<p>
					<input type="submit" value="Получить информацию" class="modal--submit" style="margin-bottom: 35px;">
					<span class="ajax-loader"></span>
					</p>
				</form>
			  </div>
			</div>
		  </div>
		</div>
		<script>  
			function get_info_status() {
				$("#result-status").html("Подождите идёт загрузка данных ...");
				  var id = $("#the_order_id").val();
			 
				$.getJSON('/getinfo.php', {id: id}, show_result_info);
				return false;
			}

			function show_result_info(data) {

				var xid = data['zayavka_id'];

				console.log(xid);

				var html = "";
					html += ("Заявка: "+data['zayavka_id']+"<br />");
					html += ("Начало: "+data['super_date']+"<br />");
					html += ("Конец: "+data['time']+"<br />");
					html += ("Место подачи: "+data['distanation_ot']+"<br />");
					html += ("Гос.номер: "+data['gos_number']+"<br />");
					html += ("Водитель: "+data['voditelname']+"<br />");
					html += ("Телефон: "+data['voditelphone']+"<br />");

				if (xid=='') {
					$("#result-status").html('Неправильный № заявки');
			 	} else {
					$("#result-status").html(html);
				}
			} 
			</script>
		
		<div class="modal modal--custom-big modal--custom" id="send_ceo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-dialog-centered bd-example-modal-lg" role="document">
			<div class="modal-content">			 
			  <a title="Close" class="modal--close" href="javascript:;" data-dismiss="modal" aria-label="Close"></a>
			  <div class="modal-body">
				<?php echo do_shortcode( '[contact-form-7 id="100" title="Написать директору"]' ); ?>
			  </div>
			</div>
		  </div>
		</div>
		
		<?php wp_footer(); ?>


	</body>

</html>
