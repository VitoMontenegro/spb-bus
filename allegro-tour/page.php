<?php get_header(); ?>
	<?php if (get_query_var('taxonomy') == 'product_cat'): ?>
		<section class="container">
			<div class="row">
				<?php get_sidebar(); ?>
				<div class="content--body col-lg-9">
					<?php if($_SERVER['REQUEST_URI'] != '/'): ?>
						<div class="breadcrumbs">
							<a href="/">Автопарк</a> — 
							<span><?php the_title();?></span>
						</div>
					<?php endif; ?>
					<h1><?php if (get_field('h1')) echo get_field('h1'); else the_title();?></h1>
					<?php if (get_field('custom-desc')) echo get_field('custom-desc');?>
					<?php the_post(); ?>
					<?php the_content(); ?>

					<?php if(get_field('текст_под_каталогом')){
						echo '<div class="clearfix"></div><div class="block-after-cat">';
							echo get_field('текст_под_каталогом');
						echo '</div>';
					} ?>

				</div>
			</div>
		</section>
	<?php else: ?>
		<section class="container">
			<div class="row">

				<?php get_sidebar(); ?>

				<div class="content--body col-lg-9">
					<?php if($_SERVER['REQUEST_URI'] != '/'): ?>
						<div class="breadcrumbs">
							<a href="/">Автопарк</a> — 
							<span><?php the_title();?></span>
						</div>
					<?php endif; ?>
					<h1><?php if (get_field('h1')) echo get_field('h1'); else the_title();?></h1>					
					<?php the_post(); ?>
					<?php the_content(); ?>
					<?php if($_SERVER['REQUEST_URI'] == '/'): ?>
						<?php
				        	$clients = get_field('clients');
				        ?>
				        <?php if ($clients): ?>
						    <style>

						        .frontpage-clients {
						            background:transparent;
						            padding-top:27px;
						            text-align:center;
						        }

						        .clients-list {
						            width:95%;
						            margin:30px auto;
						            text-align:left;
						            font-size:0;
						        }

						        .clients-list a {
						            display:inline-block;
						            width:20%;
						            height:120px;
						            background-position:center center;
						            background-repeat:no-repeat;
						            background-size:85%;
						        }
						    </style>
						    <div class="block frontpage-clients">
						         <h2 class="text-center mt-3">Нам доверяют</h2>
						        <div class="clients-list multiple-items">
						            <?php foreach ($clients as $client) : ?>
						            <!--a href="<?php echo $client['url']; ?>" target="_blank" style="background-image:url('<?php echo $client['logo']['sizes']['medium']; ?>')"></a-->
						            <img src="<?php echo $client['logo']['sizes']['medium']; ?>" alt="">
						            <?php endforeach; ?>
						        </div>
						    </div>				        	
				        <?php endif ?>
					    <div class="shadowed block frontpage-reviews">
					        <div class="inner">
					            <h2 class="text-center mt-3">Отзывы</h2>
					            <div class="reviews-carousel">
					                <div class="carousel slide">
					                    <div class="carousel-inner" step="1">
					                        <?php
					                            $args = array(
					                                'posts_per_page' => '30',
					                                'post_type' => 'reviews'
					                            );
					                            $myposts = get_posts( $args );
					                            foreach( $myposts as $post ){ setup_postdata($post);
					                            ?>
					                                <div class="carousel-item">
					                                    <?php the_content(); ?>
					                                    <div class="review-author"><?php the_title(); ?></div>
					                                </div>
					                            <?php
					                            }
					                        wp_reset_postdata(); // сбрасываем переменную $post
					                        ?>
					                    </div>
					                </div>
					                <div class="reviews-control">
					                    <div class="carousel-control-prev"></div>
					                    <div class="reviews-count"><span>1</span>/30</div>
					                    <div class="carousel-control-next"></div>
					                </div>
					            </div>
					            <?php /* <a href="/otzyvy" class="reviews-link">Все отзывы</a> */ ?>
					        </div>
					    </div>					    
					    <script>
					        (function($) {
					            $(document).ready(function() {
									$('.multiple-items').slick({
									  dots: false,
									  infinite: false,
									  speed: 300,
									  slidesToShow: 4,
									  slidesToScroll: 4,
									  responsive: [
									    {
									      breakpoint: 1024,
									      settings: {
									        slidesToShow: 3,
									        slidesToScroll: 3,
									        infinite: true,
									        dots: false
									      }
									    },
									    {
									      breakpoint: 600,
									      settings: {
									        slidesToShow: 2,
									        slidesToScroll: 2
									      }
									    },
									    {
									      breakpoint: 480,
									      settings: {
									        slidesToShow: 1,
									        slidesToScroll: 1
									      }
									    }
									    // You can unslick at a given breakpoint now by adding:
									    // settings: "unslick"
									    // instead of a settings object
									  ]
									});
					                $('.carousel-inner').each(function() {
					                    var img_count = $(this).find('.carousel-item').length;
					                    $(this).attr('steps', img_count);
					                    var slider_width = img_count * 100;
					                    $(this).css('width', slider_width + "%");
					                    $(this).find('.carousel-item').each(function() {
					                        var img_width = 100 / img_count;
					                        $(this).css('width', img_width + "%");
					                    });
					                });
					                $('.carousel').css('opacity', '1');
					                $('.carousel-control-next').on('click', function() {
					                    var curr_step = $(this).closest('.reviews-carousel').find('.carousel-inner').attr('step');
					                    var slide_steps = $(this).closest('.reviews-carousel').find('.carousel-inner').attr('steps');
					                    var step_perc = curr_step * (100 / slide_steps);
					                    if (curr_step != slide_steps) {
					                        $(this).closest('.reviews-carousel').find('.carousel-inner .carousel-item:first').css('margin-left', '-' + step_perc + '%');
					                        curr_step++;
					                        $(this).closest('.reviews-carousel').find('.carousel-inner').attr('step', curr_step);
					                        $('.reviews-count span').text(curr_step);
					                    } else {
					                        $(this).closest('.reviews-carousel').find('.carousel-inner .carousel-item:first').css('margin-left', '0');
					                        $(this).closest('.reviews-carousel').find('.carousel-inner').attr('step', '1');
					                        $('.reviews-count span').text('1');
					                    }
					                });
					                $('.carousel-control-prev').on('click', function() {
					                    var curr_step = $(this).closest('.reviews-carousel').find('.carousel-inner').attr('step');
					                    var slide_steps = $(this).closest('.reviews-carousel').find('.carousel-inner').attr('steps');
					                    var step_perc = (curr_step * (100 / slide_steps)) - (100 / slide_steps) - (100 / slide_steps);
					                    var max_step_perc = 100 - (curr_step * (100 / slide_steps));
					                    if (curr_step == 1) {
					                        $(this).closest('.reviews-carousel').find('.carousel-inner .carousel-item:first').css('margin-left', '-' + max_step_perc + '%');
					                        $(this).closest('.reviews-carousel').find('.carousel-inner').attr('step', slide_steps);
					                        $('.reviews-count span').text(slide_steps);
					                    } else {
					                        $(this).closest('.reviews-carousel').find('.carousel-inner .carousel-item:first').css('margin-left', '-' + step_perc + '%');
					                        curr_step--;
					                        $(this).closest('.reviews-carousel').find('.carousel-inner').attr('step', curr_step);
					                        $('.reviews-count span').text(curr_step);
					                    }
					                });
					            });
					        })( jQuery );
					    </script>
					    

					<?php endif; ?>
				</div>
			</div>
		</section>
	<?php endif; ?>
		
		<section class="container text_content">
			<div class="row">
				<div class="col-12">
					<div class="text_content--body">
						<?php if(get_field('text_content')){
							the_field('text_content');
						}?>
					</div>
				</div>
			</div>
		</section>



<?php get_footer(); ?>